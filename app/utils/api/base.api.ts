/* eslint global-require: off, no-console: off */

/**
 * Base API Class to make API Calls to Stock APIs
 */

import { Stock } from '../interfaces';

export default abstract class APIBase {
  /** API Key */
  apiKey: string;

  /** Companies */
  companies: any;

  /** Stocks */
  stocks: Stock[];

  testString: string;

  /**
   * Constructor
   * @param key api key
   */
  constructor(key: string) {
    this.apiKey = key;
    this.stocks = [];
  }

  /**
   * Update single stock
   * @param string company symbol to update
   */
  abstract async get(symbol: string): Promise<Stock>;

  /**
   * Update All Stock Listed in companies json file
   * @return Promise<Stock[]>
   */
  abstract async getAll(symbols: string[]): Promise<Stock[]>;
}
