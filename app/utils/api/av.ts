/* eslint global-require: off, no-console: off */

/**
 * API Functions for alphavantage
 */

import axios from 'axios';
import { Stock } from '../interfaces';
import APIBase from './base.api';
import twoDecimalPlaces from '../functions';

export default class AV extends APIBase {
  /**
   * Get single stock information
   * @param string company symbol
   * @return Promise<Stock> Desired stock
   */
  async get(symbol: string): Promise<Stock> {
    let stock: Stock;

    await axios
      .get(
        `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${symbol}&apikey=${this.apiKey}`
      )
      // eslint-disable-next-line promise/always-return
      .then((data: any) => {
        // Add stock to array
        stock = {
          company: symbol,
          price: twoDecimalPlaces(data.data['Global Quote']['05. price']),
          high: twoDecimalPlaces(data.data['Global Quote']['03. high']),
          low: twoDecimalPlaces(data.data['Global Quote']['04. low'])
        };
      })
      .catch((error: Error) => {
        console.log(error);
        throw new Error(error.message);
      });

    return stock;
  }

  /**
   * Return all desired stocks from API
   * @return Promise<Stock[]> Desired Stock Information
   */
  async getAll(symbols: string[]): Promise<Stock[]> {
    await Promise.all(
      symbols.map(async (symbol: any) => {
        // API Call & Add to stock list
        await axios
          .get(
            `https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=${symbol}&apikey=${this.apiKey}`
          )
          // eslint-disable-next-line promise/always-return
          .then((data: any) => {
            // Add stock to array
            this.stocks.push({
              company: symbol,
              price: twoDecimalPlaces(data.data['Global Quote']['05. price']),
              high: twoDecimalPlaces(data.data['Global Quote']['03. high']),
              low: twoDecimalPlaces(data.data['Global Quote']['04. low'])
            });
          })
          .catch((error: Error) => {
            console.log(error);
            throw new Error(error.message);
          });
      })
    );

    return this.stocks;
  }
}
