/* eslint global-require: off, no-console: off */

/**
 * API Functions for fmpcloud.io
 */

import axios from 'axios';
import { Stock } from '../interfaces';
import APIBase from './base.api';
import twoDecimalPlaces from '../functions';

export default class FMP extends APIBase {
  /**
   * Get single stock information
   * @param string company symbol
   * @return Promise<Stock> Desired stock
   */
  async get(symbol: string): Promise<Stock> {
    let stock: Stock;

    await axios
      .get(`https://fmpcloud.io/api/v3/quote/${symbol}?apikey=${this.apiKey}`)
      // eslint-disable-next-line promise/always-return
      .then((data: any) => {
        // Add stock to array
        stock = {
          company: symbol,
          price: twoDecimalPlaces(data.data[0].price),
          high: twoDecimalPlaces(data.data[0].dayHigh),
          low: twoDecimalPlaces(data.data[0].dayLow)
        };
      })
      .catch((error: Error) => {
        console.log(error);
        throw new Error(error.message);
      });

    return stock;
  }

  /**
   * Return all desired stocks from API
   * @return Promise<Stock[]> Desired Stock Information
   */
  async getAll(symbols: string[]): Promise<Stock[]> {
    // API Call & Add to stock list
    await axios
      .get(
        `https://fmpcloud.io/api/v3/quote/${symbols.join(',')}?apikey=${
          this.apiKey
        }`
      )
      // eslint-disable-next-line promise/always-return
      .then((data: any) => {
        data.data.forEach((stock: any) => {
          // Add eacn stock to array
          this.stocks.push({
            company: stock.symbol,
            price: twoDecimalPlaces(stock.price),
            high: twoDecimalPlaces(stock.dayHigh),
            low: twoDecimalPlaces(stock.dayLow)
          });
        });
      })
      .catch((error: Error) => {
        console.log(error);
        throw new Error(error.message);
      });

    return this.stocks;
  }
}
