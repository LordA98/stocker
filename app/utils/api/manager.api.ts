/* eslint global-require: off, no-console: off */

/**
 * API Manager Class to handle requests to the 3 different APIs
 */

import FMP from './fmp';
import AV from './av';
import FH from './fh';
import { Stock } from '../interfaces';

export default class APIManager {
  /** AlphaVantage API Key */
  avKey: string;

  /** FmpCloud API Key */
  fmpKey: string;

  /** Finnhub API Key */
  fhKey: string;

  /** Active API */
  activeApi: AV | FMP | FH;

  /**
   * Constructor
   * Set API Keys
   */
  constructor() {
    this.avKey = process.env.AV_API_KEY;
    this.fmpKey = process.env.FMP_API_KEY;
    this.fhKey = process.env.FH_API_KEY;
  }

  /**
   * Update single stock information in context
   * TODO: figure out if it needs to be async (possibly if it needs to return something to caller)
   * @param string api to use
   * @param string symbol of company to update
   */
  async get(api: string, symbol: string): Promise<Stock> {
    switch (api) {
      case 'av':
        this.activeApi = new AV(process.env.AV_API_KEY);
        break;
      case 'fmp':
        this.activeApi = new FMP(process.env.FMP_API_KEY_TWO);
        break;
      case 'fh':
        this.activeApi = new FH(process.env.FH_API_KEY);
        break;
      default:
        this.activeApi = new FH(process.env.FH_API_KEY);
        break;
    }

    return this.getSingleStock(symbol);
  }

  /**
   * Make API Call to Finnhub to get single stock information
   * @param string symbol of company
   */
  private async getSingleStock(symbol: string): Promise<Stock> {
    let stock: Stock;

    await this.activeApi
      .get(symbol)
      // eslint-disable-next-line promise/always-return
      .then((s: Stock) => {
        stock = s;
      })
      .catch((error: Error) => {
        // TODO: Bubble error up & add to API error count
        console.log(error);
      });

    return stock;
  }

  /**
   * Update batch stock information in context
   * TODO: figure out if it needs to be async (possibly if it needs to return something to caller)
   * @param string api to use
   * @param array symbols of companies to update
   */
  async getMultiple(api: string, symbol: string[]): Promise<Stock[]> {
    switch (api) {
      case 'av':
        this.activeApi = new AV(process.env.AV_API_KEY);
        break;
      case 'fmp':
        this.activeApi = new FMP(process.env.FMP_API_KEY_TWO);
        break;
      case 'fh':
        this.activeApi = new FH(process.env.FH_API_KEY);
        break;
      default:
        this.activeApi = new FMP(process.env.FMP_API_KEY_TWO);
        break;
    }

    return this.getMultipleStocks(symbol);
  }

  /**
   * Make API Call to Finnhub to get single stock information
   * @param string symbol of company
   */
  private async getMultipleStocks(symbols: string[]): Promise<Stock[]> {
    let stocks: Stock[];

    await this.activeApi
      .getAll(symbols)
      // eslint-disable-next-line promise/always-return
      .then((s: Stock[]) => {
        stocks = s;
      })
      .catch((error: Error) => {
        // TODO: Bubble error up & add to API error count
        console.log(error);
      });

    return stocks;
  }
}
