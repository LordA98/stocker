/**
 * Interfaces for App
 */

export interface Stock {
  company: string;
  price: number;
  high?: number;
  low?: number;
}
