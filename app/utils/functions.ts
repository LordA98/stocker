/**
 * Round prices to 2 decimal place
 * @return number to 2 d.p.
 */
export default function twoDecimalPlaces(toConvert: string): number {
  return Number((Math.round(Number(toConvert) * 100) / 100).toFixed(2));
}
