import React from 'react';
import { hot } from 'react-hot-loader/root';
import Routes from '../Routes';
import { StockProvider } from '../context/StockState';

// TODO: Remove root & routes & replace with App

const Root = () => (
  <StockProvider>
    <Routes />
  </StockProvider>
);

export default hot(Root);
