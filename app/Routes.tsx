import React from 'react';
import App from './containers/App';
import HomePage from './containers/HomePage';

export default function Routes() {
  return (
    <App>
      <HomePage />
    </App>
  );
}
