/**
 * App Reducer for Context API
 */

export default (state: any, action: any) => {
  switch (action.type) {
    case 'ADD_STOCK':
      return {
        ...state,
        stocks: [action.payload, ...state.stocks]
      };
    case 'UPDATE_STOCK':
      return {
        ...state,
        stocks: state.stocks.map((stock: any, index: number) => {
          if (stock.company === action.payload.company) {
            return {
              ...stock,
              price: action.payload.price,
              high: action.payload.high,
              low: action.payload.low
            };
          }
          return stock;
        })
      };
    case 'DELETE_STOCK':
      return {
        ...state,
        stocks: state.stocks.filter(
          (stock: any) => stock.company !== action.payload
        )
      };
    default:
      return state;
  }
};
