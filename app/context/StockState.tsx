/**
 * Context API - Stock State
 */

import React, { createContext, useReducer } from 'react';
import RootReducer from './RootReducer';
import { Stock } from '../utils/interfaces';

// Initial State
const initialStockState = {
  // [] as Stock[]
  stocks: [
    {
      company: 'AAPL',
      price: 0.0,
      high: 0.0,
      low: 0.0
    },
    {
      company: 'TSLA',
      price: 0.0,
      high: 0.0,
      low: 0.0
    },
    {
      company: 'FB',
      price: 0.0,
      high: 0.0,
      low: 0.0
    },
    {
      company: 'AMZN',
      price: 0.0,
      high: 0.0,
      low: 0.0
    }
  ]
};

// Interface for stock state
interface StockState {
  /** Properties */
  stocks: Stock[];
  /** Methods */
  addStock: (stock: Stock) => any;
  updateStock: (stock: Stock) => any;
  deleteStock: (company: string) => any;
}

// Export default state
export const StockContext = createContext<Partial<StockState>>(
  initialStockState
);

// Props type
type Props = {
  children: React.ReactNode;
};

// Provider component
export const StockProvider = ({ children }: Props) => {
  const [state, dispatch] = useReducer(RootReducer, initialStockState);

  // Add stock to global context
  function addStock(stock: Stock) {
    dispatch({
      type: 'ADD_STOCK',
      payload: stock
    });
  }

  // Update stock already in global context
  function updateStock(stock: Stock) {
    dispatch({
      type: 'UPDATE_STOCK',
      payload: stock
    });
  }

  // Delete stock from global context
  function deleteStock(company: string) {
    dispatch({
      type: 'DELETE_STOCK',
      payload: company
    });
  }

  // Return provider to wrap around root component
  return (
    <StockContext.Provider
      value={{ stocks: state.stocks, deleteStock, addStock, updateStock }}
    >
      {children}
    </StockContext.Provider>
  );
};
