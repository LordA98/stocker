/* eslint-disable no-console */
import React, { useContext } from 'react';
import { useForm } from 'react-hook-form';
import { StockContext } from '../../context/StockState';
import { Stock } from '../../utils/interfaces';
import APIManager from '../../utils/api/manager.api';

export default function AddStock() {
  const am = new APIManager();

  const { addStock } = useContext(StockContext);

  const { register, handleSubmit } = useForm<Inputs>();

  const onSubmit = async (data: any) => {
    const stock: Stock = await am.get('fh', data.symbol);
    addStock(stock);
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="montserrat-light w-full max-w-sm mx-auto"
    >
      <div className="flex items-center">
        <input
          name="symbol"
          className="flex p-3 uppercase rounded-l-lg placeholder-white placeholder-opacity-50 tracking-wider bg-greysecondary w-full text-white focus:outline-none focus:bg-white focus:text-black"
          type="text"
          placeholder="ENTER STOCK"
          aria-label="Stock Symbol"
          ref={register({ required: true })}
        />
        <button
          className="py-3 px-5 uppercase rounded-r-lg tracking-wider bg-burgundy text-white hover:bg-burgundysecondary"
          type="submit"
        >
          Add
        </button>
      </div>
    </form>
  );
}
