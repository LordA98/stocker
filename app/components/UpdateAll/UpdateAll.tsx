/* eslint-disable no-console */
import React, { useContext } from 'react';
import { StockContext } from '../../context/StockState';
import { Stock } from '../../utils/interfaces';
import APIManager from '../../utils/api/manager.api';

/**
 * TODO: Future Changes:
 * - Cater for Finnhub 60/min API call limit (if there are more than 60 stocks)
 */

export default function UpdateAll() {
  const am = new APIManager();

  const { updateStock } = useContext(StockContext);
  const { stocks } = useContext(StockContext);

  const onClick = async () => {
    const symbols: string[] = stocks.map((stock: Stock) => {
      return stock.company;
    });
    const tmp: Stock[] = await am.getMultiple('fh', symbols);
    tmp.map((stock: Stock) => updateStock(stock));
  };

  return (
    <>
      <button
        className="py-1 px-4 flex mt-5 mx-auto montserrat-light uppercase text-xs rounded-lg bg-green hover:bg-greensecondary"
        type="submit"
        onClick={onClick}
      >
        Update All
      </button>

      <div className="flex mt-1">
        <span className="mx-auto text-white text-opacity-25 text-xs montserrat-light">
          Oldest Stock: 00:00:00am
        </span>
      </div>
    </>
  );
}
