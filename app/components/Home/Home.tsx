/* eslint global-require: off, no-console: off */

import React from 'react';
// import Sandbox from '../Sandbox/Sandbox';
import APICalls from '../APICalls/APICalls';
import AddStock from '../AddStock/AddStock';
import StockGrid from '../StockGrid/StockGrid';
import UpdateAll from '../UpdateAll/UpdateAll';

// import styles from './Home.css';

export default function Home() {
  return (
    <>
      {/* <Sandbox /> */}

      <APICalls />

      <AddStock />
      <UpdateAll />

      <StockGrid />
    </>
  );
}
