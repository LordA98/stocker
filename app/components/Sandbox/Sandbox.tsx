/* eslint global-require: off, no-console: off */

/**
 * Used to test functionality - primarily API calling
 */

import React, { useEffect, useContext } from 'react';
import ReactDOM from 'react-dom';
import { Stock } from '../../utils/interfaces';
import { StockContext } from '../../context/StockState';
import APIManager from '../../utils/api/manager.api';

export default function Sandbox(this: any) {
  const am = new APIManager();

  const { stocks } = useContext(StockContext);
  const { addStock } = useContext(StockContext);
  const { updateStock } = useContext(StockContext);
  const { deleteStock } = useContext(StockContext);

  useEffect(() => {
    console.log('Use Effect');
    console.log(stocks);
    console.log(process.env.FH_API_KEY);
  }, []);

  async function update(e: any) {
    e.preventDefault();
    const aapl = await am.get('fh', 'AAPL');
    updateStock(aapl);
  }

  async function add(e: any) {
    e.preventDefault();
    const tsla = await am.get('fh', 'TSLA');
    addStock(tsla);
  }

  async function remove(e: any) {
    e.preventDefault();
    deleteStock('AAPL');
  }

  async function addMultiple(e: any) {
    e.preventDefault();
    const s = await am.getMultiple('fh', ['FB', 'SNAP']);
    ReactDOM.unstable_batchedUpdates(() => {
      s.forEach((stock: any) => {
        addStock(stock);
      });
    });
  }

  return (
    <div>
      <h2>Sandbox</h2>

      <p>
        <b>Context (Stocks):</b>
      </p>

      {stocks.map((stock: Stock) => (
        <div key={stock.company}>
          {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
          {stock.company} : {stock.price} : {stock.high} : {stock.low}
        </div>
      ))}

      <br />

      <button type="button" onClick={e => add(e)}>
        Add TSLA
      </button>
      <button type="button" onClick={e => update(e)}>
        Update AAPL
      </button>
      <button type="button" onClick={e => remove(e)}>
        Delete AAPL
      </button>

      <button type="button" onClick={e => addMultiple(e)}>
        Add FB & SNAP
      </button>
    </div>
  );
}
