import React, { useContext } from 'react';
import StockItem from '../StockItem/StockItem';
import { StockContext } from '../../context/StockState';
import { Stock } from '../../utils/interfaces';

export default function StockGrid() {
  const { stocks } = useContext(StockContext);

  return (
    <div>
      <h3>Stock Grid Component</h3>

      <StockItem />

      <br />
      <br />

      <p>Context (Stocks):</p>

      {stocks.map((stock: Stock) => (
        <div key={stock.company}>
          {/* eslint-disable-next-line react/jsx-one-expression-per-line */}
          {stock.company} : {stock.price} : {stock.high} : {stock.low}
        </div>
      ))}
    </div>
  );
}
