module.exports = {
  important: true,
  theme: {
    extend: {
      colors: {
        green: '#00A550',
        greensecondary: '#00833F',
        burgundy: '#70000F',
        burgundysecondary: '#52020D',
        grey: '#333333',
        greysecondary: '#292929',
        white: '#FFF'
      }
    }
  },
  variants: {},
  plugins: []
};
